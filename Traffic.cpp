/* ------------------------------------------------
 *
 * Author: Alexander Oey
 *
 * -------------------------------------------------
 */
#include <iostream>
#include <fstream> //File input.
#include <vector>
#include <cctype> //toupper()
#include <string> //string
#include <algorithm> //std::fill()
#include "gobjects.h"  // for all graphics objects
using namespace std;


/* Classes */
//Vehicle class declaration and definition.
class Vehicle {
    private: //Internal data members.
        char vehicleType; //Correspond to the color of the vehicle.
        char direction; //Horizontal(H) or Vertical(V).
        int rowNum; //Vehicle row position.
        int colNum; //Vehicle column position.
        int length; //Length in points.
        string color; //Color of vehicle based on vehicleType.

    public: //Class public functions.
        //Constructors
        Vehicle() {} //Empty.
        Vehicle(char vehicleType, char direction, int rowNum, int colNum, int length) {
            setVehicleType(vehicleType);
            setMovementDirection(direction);
            setRow(rowNum);
            setColumn(colNum);
            setLength(length);
            setColor(vehicleType);
        }

        //Setters.
        void setVehicleType(char vehicleType) {
            this->vehicleType = vehicleType;
        }
        void setMovementDirection(char direction) {
            this->direction = direction;
        }
        void setRow(int rowNum){
            this->rowNum = rowNum;
        }
        void setColumn(int colNum) {
            this->colNum = colNum;
        }
        void setLength(int length) {
            this->length = length;
        }
        void setColor(char vehicletype); //Defined outside the class

        //Getters.
        char getVehicleType() const {
            return vehicleType;
        }
        char getMovementDirection() const {
            return direction;
        }
        int getRow() const {
            return rowNum;
        }
        int getColumn() const {
            return colNum;
        }
        int getLength() const {
            return length;
        }
        string getColor() const {
            return color;
        }

        //Move vehicle.
        bool move(char direction); //Defined outside class.

        //Operator overload >>.
        friend std::istream& operator>>(std::istream& is, Vehicle& assign) {
            //Put information into appropriate fields.
            is >> assign.vehicleType;
            is >> assign.direction;
            is >> assign.rowNum;
            is >> assign.colNum;
            is >> assign.length;
            assign.setColor(assign.vehicleType);

            return is;
        }
};


//Class function definition of setColor.
void Vehicle::setColor(char vehicleType) {
    switch(vehicleType) {
        case 'G':   color = "green";    break;
        case 'Y':   color = "yellow";   break;
        case 'M':   color = "magenta";  break;
        case 'R':   color = "red";      break;
        case 'B':   color = "blue";     break;
        case 'O':   color = "orange";   break;
        case 'C':   color = "cyan";     break;
        case 'P':   color = "pink";     break;
        default: //Undefined color.
            cout << "Error! Color not defined." << endl;
            break;
    }
}


//Class function defintion of move.
//Move vehicle and check for invalid row and column positions.
bool Vehicle::move(char direction) {
    if (direction == 'U' && rowNum > 1) { //Up.
        --rowNum;
        return true;
    }
    else if (direction == 'D' && rowNum + (length - 1) < 6) { //Down.
        ++rowNum;
        return true;
    }
    else if (direction == 'L' && colNum > 1) { //Left.
        --colNum;
        return true;
    }
    else if (direction == 'R' && colNum + (length - 1) < 6) { //Right.
        ++colNum;
        return true;
    }
    else { //Movement failed.
        return false;
    }
}


/* Global functions */
// Display ID info.
void displayIDInfo() {
    cout << "Author: Alexander Oey" << endl;
    cout << "Lab: Tues 11am (TA: Minh Huynh Nguyen)" << endl;
    cout << "Program #4: Graphical Traffic" << endl << endl;
}


// Display Instructions.
void displayInstructions() {
    cout << "Welcome to the traffic game!                          " << endl
         << "                                                      " << endl
         << "Move the vehicles so that the Red car (RR) can exit   " << endl
         << "the board to the right. Each move should be of the    " << endl
         << "of the form:   CDN   where:                           " << endl
         << "   C  is the vehicle to be moved                      " << endl
         << "   D  is the direction (u=up, d=down, l=left or r=right)" << endl
         << "   N  is the number of squares to move it             " << endl
         << "For example GR2  means move the G vehicle to the right" << endl
         << "2 squares.  Lower-case input such as   gr2  is also   " << endl
         << "accepted.  Enter \"reset\" to reset board, or \"exit\" to exit.  " << endl;
}


//Draw board.
void drawGraphicalBoard(vector<Vehicle> &vehicleInfo, GWindow &gameWindow) {
    //Draw the outer border of the board.
    GRect outerBorder(10, 10, 270, 270);
    outerBorder.setFilled(true);
    outerBorder.setColor("darkGray");
    gameWindow.add(&outerBorder);

    //Draw the inner playing surface.
    GRect innerBorder(20, 20, 250, 250);
    innerBorder.setFilled(true);
    innerBorder.setColor("gray");
    gameWindow.add(&innerBorder);

    //Draw the opening for the exit point.
    GRect exitOpening(270, 100, 10, 50);
    exitOpening.setFilled(true);
    exitOpening.setColor("lightGray");
    gameWindow.add(&exitOpening);

    //Draw lines to create square plots.
    for (int i = 0; i < 7; ++i) {
        GRect verticalLine(20 + i * 40, 20, 10, 250);
        verticalLine.setFilled(true);
        verticalLine.setColor("lightGray");
        gameWindow.add(&verticalLine);
        GRect horizontalLine(20, 20 + i * 40, 250, 10);
        horizontalLine.setFilled(true);
        horizontalLine.setColor("lightGray");
        gameWindow.add(&horizontalLine);
    }

    //Draw cars.
    for (Vehicle const &vehicle: vehicleInfo) {
        if (vehicle.getMovementDirection() == 'H'){
            GRect car((vehicle.getColumn()-1) * 40 + 30, (vehicle.getRow()-1) * 40 + 30,
                      vehicle.getLength() * 40 - 10, 30);
            car.setFilled(true);
            car.setColor(vehicle.getColor());
            gameWindow.add(&car);
        }
        else {
            GRect car((vehicle.getColumn()-1) * 40 + 30, (vehicle.getRow()-1) * 40 + 30,
                      30, vehicle.getLength() * 40 - 10);
            car.setFilled(true);
            car.setColor(vehicle.getColor());
            gameWindow.add(&car);
        }

    }
}


//Read from file and store data into vehicleInfo vector.
void readFile(vector<Vehicle> &vehicleInfo) {
    //Setting up input stream.
    ifstream fileInputStream; //Create file input stream object.
    fileInputStream.open("board.txt");

    //Check if file fails to open.
    if (!fileInputStream) {
        cerr << "Unable to open board.txt";
        exit(1); //Exit program with errors.
    }

    //Helper variable declaration.
    int numEntries; //Number of vehicles in the file.
    Vehicle temp; //Temporary for pushing to vehicleInfo vector.

    //Read from stream.
    fileInputStream >> numEntries;
    while(fileInputStream >> temp) {
        vehicleInfo.push_back(temp);
    }

    //Close input stream.
    fileInputStream.close();
}


//Return pointer to a Vehicle object in vector vehicleInfo given userVehicle.
Vehicle* getVehiclePointer(char userVehicle, vector<Vehicle> &vehicleInfo) {
    //Check for action item validity.
    for (int i = 0; i < vehicleInfo.size(); ++i) {
        if (userVehicle == vehicleInfo.at(i).getVehicleType()) { //Vehicle found.
            return &vehicleInfo.at(i); //Return reference to the vehicle.
        }
    }
    return NULL; //Vehicle not found.
}


//Check if the direction is valid for the selected piece.
bool verifyDirection(char userDirection, Vehicle* selectedPiece) {
    char generalDirection; //Vertical 'V' or Horizontal 'H'.

    //Convert direction to general direction.
    switch (userDirection) {
        case 'U': //Up
        case 'D': //Down.
            generalDirection = 'V'; //Vertical.
            break;
        case 'R': //Right.
        case 'L': //Left.
            generalDirection = 'H'; //Horizontal.
            break;
        default: //Invalid direction.
            return false;
    }

    return (selectedPiece->getMovementDirection() == generalDirection);
}


//Check if the intended path of selected piece is clear.
bool checkPath(string userInput, vector<char> &board, Vehicle* selectedPiece) {
    char userDirection = toupper(userInput.at(1)); //Vehicle movement direction.
    int userDistance = userInput.at(2) - '0'; //Distance of vehicle movement.
    int initialRow = selectedPiece->getRow(); //Current position of selected piece.
    int initialCol = selectedPiece->getColumn(); //Current position of selected piece.
    int headIndex = 0; //Index of the front part of the vehicle relative to the movement direction.

    //Check every step of the intended movement.
    for (int i = 1; i <= userDistance; ++i) {
        //Move the vehicle and check for moving piece off the edge.
        if (!selectedPiece->move(userDirection)) { //Fail to move vehicle.
            selectedPiece->setRow(initialRow); //Revert the rowNum of piece.
            selectedPiece->setColumn(initialCol); //Revert the colNum of piece.
            cout << "Attempt to move the piece off the edge of the board." << endl;
            return false; //Error.
        }

        //Check for moving piece on top of another piece.
        //Default case is moving up and left.
        headIndex = (selectedPiece->getRow() - 1) * 6 + (selectedPiece->getColumn() - 1);
        //Set headIndex for some other cases.
        if (userDirection == 'R') { //Right.
            headIndex += selectedPiece->getLength() - 1; //Add length to col.
        }
        else if (userDirection == 'D') { //Down.
            headIndex += (selectedPiece->getLength() - 1) * 6; //Add length to row.
        }

        //Check whether the headIndex, after moving 1 step, is occupied.
        if (board.at(headIndex) != '.' && headIndex < 36) { //Occupied.
            selectedPiece->setRow(initialRow); //Revert the rowNum of piece.
            selectedPiece->setColumn(initialCol); //Revert the colNum of piece.
            cout << "Attempt to move a piece on top of another piece." << endl;
            return false; //Error.
        }
    }
    return true; //No errors.
}


//Write data from vehicleInfo to the text-based board.
void writeToTextBoard(vector <char> &board, vector<Vehicle> &vehicleInfo) {
    //Wipe text based board.
    fill(board.begin(), board.end(), '.');

    //Write vehicle coordinate data to text-based board.
    for (Vehicle const &vehicle: vehicleInfo) { //Go through all elements in vehicleInfo.
        int startIndex = (vehicle.getRow() - 1) * 6 + (vehicle.getColumn() - 1);
        for (int i = 0; i < vehicle.getLength(); ++i) {
            if (vehicle.getMovementDirection() == 'H') { //Horizontal.
                board.at(startIndex+i) = vehicle.getVehicleType();
            }
            else { //Vertical
                board.at(startIndex + (6 * i)) = vehicle.getVehicleType();
            }
        }
    }
}


//Check for winning condition.
bool checkWin(vector<char> &board) {
    return (board.at(17) == 'R');
}


//Reset the board.
void resetBoard(vector<char> &board, vector<Vehicle> &vehicleInfo) {
    //Clear vehicleInfo.
    vehicleInfo.clear();

    //Take user input for new board values.
    int numEntries = 0; //Number of vehicles to be placed.
    Vehicle temp; //Temporary for pushing to vehicleInfo vector.

    cout << "Enter number of vehicles: ";
    cin >> numEntries;
    cout << "Enter vehicle, movement direction, rows, columns, and length: " << endl;

    //Add user input to vector vehicleInfo.
    for (int i = 0; i < numEntries; ++i) {
        cin >> temp;
        vehicleInfo.push_back(temp);
    }

    //Write to text-based board.
    writeToTextBoard(board, vehicleInfo);
}


//Main
int main() {
    //Variable declaration
    string userInput; //Stores user input.
    vector<char> boardSquares(36); //Board configuration.
    vector<Vehicle> vehicleInfo; //Information about pieces.
    Vehicle* selectedPiece; //Pointer to selected vehicle.
    int moveCount = 0; //Number of moves user has taken.

    //Print program information.
    displayIDInfo();
    displayInstructions();

    //Create graphics windows to be used as a container.
    GWindow gameWindow(300, 300); //Set size.
    gameWindow.setTitle("CS 141: Prog 4 Graphical Traffic");

    //Read from file.
    readFile(vehicleInfo);
    writeToTextBoard(boardSquares, vehicleInfo);

    //Game logic
    while(true) { //Infinite loop.
        //Paint board.
        drawGraphicalBoard(vehicleInfo, gameWindow);

        //Get user input.
        cout << ++moveCount <<  ". Your userInput: ";
        cin >> userInput;
        if (userInput == "exit") { //Exit.
            break;
        }
        else if (userInput == "reset") { //Reset.
            resetBoard(boardSquares, vehicleInfo);
            moveCount = 0;
        }
        else { //Other input.
            selectedPiece = getVehiclePointer(toupper(userInput.at(0)), vehicleInfo); //Get pointer to selected piece.
            //Validate input.
            if (selectedPiece == NULL) { //Vehicle not found.
                cout << "User input is not one of the valid color or action items. "
                     << "Please retry." << endl;
                continue;
            }
            if (!verifyDirection(toupper(userInput.at(1)), selectedPiece)) { //Invalid direction.
                cout << "Attempt to move the piece in the wrong direction. Please retry." << endl;
                continue;
            }
            if (!checkPath(userInput, boardSquares, selectedPiece)) { //Path blocked or going to invalid region.
                continue;
            }

            //Write to text-based board.
            writeToTextBoard(boardSquares, vehicleInfo);

            //Check for winning condition.
            if (checkWin(boardSquares)) { //Win.
                drawGraphicalBoard(vehicleInfo, gameWindow);
                cout << "Congratulations! You won." << endl;
                break;
            }
        }
    }

    cout << "Thank you for playing!" << endl;
    return 0;
}
